package pw.tcrs.tcrscore.sum;

import pw.tcrs.tcrscore.TcrsCore;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import cpw.mods.fml.common.registry.GameRegistry;

public class SUM_GameRegistry {

	/**
	 * FMLやModLoaderのaddRecipeと使い方は同じ
	 * @param output 作成アイテム
	 * @param params 材料のアイテム
	 */
	public static void addRecipe(ItemStack output, Object... params) {
		CraftingManager.getInstance().addRecipe(output, params);
	}

	/**
	 * FMLやModLoaderのaddShapelessRecipeと使い方は同じ
	 * @param output 作成アイテム
	 * @param params 材料のアイテム
	 */
	public static void addShapelessRecipe(ItemStack output, Object... params) {
		CraftingManager.getInstance().addShapelessRecipe(output, params);
	}
	
	/**
	 * 定形レシピ追加時に作成アイテムのレシピをすべて削除し、追加をします。
	 * @param output 作成アイテム
	 * @param params 材料のアイテム
	 */
	public static void replaceRecipe(ItemStack output, Object... params) {
		TcrsCore.DeleteCraftingRecipe(output);
		addRecipe(output, params);
	}

	/**
	 * 不定形レシピ追加時に作成アイテムのレシピをすべて削除し、追加をします。
	 * @param output 作成アイテム
	 * @param params 材料のアイテム
	 */
	public static void replaceShapelessRecipe(ItemStack output, Object... params) {
		TcrsCore.DeleteCraftingRecipe(output);
		addShapelessRecipe(output, params);
	}
	
	/**
	 * 精錬レシピの追加
	 * @param input 精錬する物のID
	 * @param output 精錬後のアイテムのItemStack
	 * @param xp 経験値の量
	 */
	public static void addSmelting(int input, ItemStack output, float xp) {
		FurnaceRecipes.smelting().addSmelting(input, output, xp);
	}
	
	/**
	 * 精錬レシピを置き換える
	 * @param input 精錬する物のItem
	 * @param output 精錬後のアイテムのItemStack
	 * @param xp 経験値の量
	 */
	public static void replaceSmelting(Item input, ItemStack output, float xp) {
		TcrsCore.DeleteSmeltingRecipe(output);
		FurnaceRecipes.smelting().addSmelting(input.itemID, output, xp);
	}
	
	/**
	 * 精錬レシピを置き換える
	 * @param input 精錬する物のBlock
	 * @param output 精錬後のアイテムのItemStack
	 * @param xp 経験値の量
	 */
	public static void replaceSmelting(Block input, ItemStack output, float xp) {
		TcrsCore.DeleteSmeltingRecipe(output);
		FurnaceRecipes.smelting().addSmelting(input.blockID, output, xp);
	}
}
