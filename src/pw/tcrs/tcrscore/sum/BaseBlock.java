package pw.tcrs.tcrscore.sum;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BaseBlock extends Block {

	public String unlocalizedName;

	public BaseBlock(int par1, Material par2Material) {
		super(par1, par2Material);
	}

	@Override
	public Block setUnlocalizedName(String par1Str) {
		unlocalizedName = par1Str;
		return this;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getUnlocalizedName() {
		return unlocalizedName;
	}

}
