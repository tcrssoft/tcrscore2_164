package pw.tcrs.tcrscore.sum;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;

public class BaseItem extends Item {

	public static int itemID;
	private String TextureName=null;
	private String unlocalizedName;

	public BaseItem(int par1) {
		super(par1);
		itemID = 256 + par1;
	}
	
	public BaseItem(int par1, String par2) {
		super(par1);
		itemID = 256 + par1;
		TextureName=par2;
	}

	@Override
	public Item setUnlocalizedName(String par1Str) {
		unlocalizedName=par1Str;
		return super.setUnlocalizedName(par1Str);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
		if(TextureName == null)
		{
			this.itemIcon = par1IconRegister.registerIcon(unlocalizedName);
		}
		else
			{
			this.itemIcon = par1IconRegister.registerIcon(this.TextureName);
			}
    }

}
