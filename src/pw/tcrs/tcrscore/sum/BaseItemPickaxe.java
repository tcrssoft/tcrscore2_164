package pw.tcrs.tcrscore.sum;

import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemPickaxe;

public class BaseItemPickaxe extends ItemPickaxe {

	public BaseItemPickaxe(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
	}

}
