package pw.tcrs.tcrscore.sum;

import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;


public interface IMOD {
	/**
	 * config設定などをここでする。
	 * ブロック・アイテムの追加処理もする。
	 * 
	 * registerItemに追加した方がすっきりするかも
	 * 
	 * registerItemを使う場合
	 * コード内にregisterItem();を記載する必要がある。
	 * 	
	 * @param event
	 */
	@EventHandler
	public void preload(FMLPreInitializationEvent Event);
	
	/**
	 * ここで、ブロック・アイテムの追加処理をする。
	 */
	public void registerItem();
	
	/**
	 * 
	 * 言語登録・レシピ追加等をここでする。
	 * 
	 * @param event
	 */
	@EventHandler
	public void load(FMLInitializationEvent event);
	
}
