package pw.tcrs.tcrscore.sum;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class BaseCreativeTab extends CreativeTabs {

	static Item IconItem;
	private String TransLabel;
	
	public BaseCreativeTab(String label, String argTransLabel) {
		super(label);
		TransLabel=argTransLabel;
		
	}
	@SideOnly(Side.CLIENT)
    public void setTabIconItem(Item ItemIcon)
    {
		IconItem=ItemIcon;
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public Item getTabIconItem()
    {
        return IconItem;
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel()
	{
		return TransLabel;
	}

}
