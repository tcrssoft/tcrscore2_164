package pw.tcrs.tcrscore;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.OreDictionary;
import pw.tcrs.killgreg.KillGreg;
import pw.tcrs.killgreg.MetalFormerRecipes;
import pw.tcrs.tcrscore.sum.IMOD;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

/**
 * TcrsSoft関係MODおよびChemiCraftの前提MOD<br>
 * <br>
 * Copyright (c) 2012～2013, TcrsSoft<br>
 * All rights reserved.<br>
 * New BSD license<br>
 * <br>
 * 
 * @author inaka
 * 
 */
@Mod(modid = "TcrsCore2.5", name = "TcrsCore2.5", version = TcrsCore.TCRSCORE_VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class TcrsCore{
	public class MODList {
		public static final String GregTech = "GregTech";
	}

	public static File configdir;
	public static final String TCRSCORE_VERSION = "2.6.164_a0";
	public static final double TCRSCORE_APIVERSION = 2.6;

	public static final String MC_VERSION = "1.6.x";

	public static void addChatMessage(String text) {

	}

	public static void addOredicSmelting(ItemStack input, String Output) {
		ArrayList<ItemStack> OreDicL = OreDictionary.getOres(Output);
		for (int i = 0; i < OreDicL.size(); i++) {
			ItemStack ItemStemp = new ItemStack(OreDicL.get(i).getItem(), 1,
					OreDicL.get(i).getItemDamage());
			String temp = ItemStemp.getUnlocalizedName();
			KillGreg.logger.log(Level.ALL, "KillGreg:" + temp);
			MetalFormerRecipes.smelting().addSmelting(input, ItemStemp, 0F);
		}
	}

	public static void addOredicSmelting(String OredicName, ItemStack Output) {
		ArrayList<ItemStack> OreDicL = OreDictionary.getOres(OredicName);
		for (int i = 0; i < OreDicL.size(); i++) {
			ItemStack ItemStemp = new ItemStack(OreDicL.get(i).getItem(), 1,
					OreDicL.get(i).getItemDamage());
			String temp = ItemStemp.getUnlocalizedName();
			KillGreg.logger.log(Level.ALL, "KillGreg:" + temp);
			MetalFormerRecipes.smelting().addSmelting(ItemStemp, Output, 0F);
		}
	}

	public static NBTTagCompound CreateFurnaceStyleRecipeData(ItemStack input,
			ItemStack output, boolean Sensitive) {
		NBTTagCompound toSend = new NBTTagCompound();
		toSend.setTag("input", new NBTTagCompound());
		toSend.setTag("output", new NBTTagCompound());
		input.writeToNBT(toSend.getCompoundTag("input"));
		output.writeToNBT(toSend.getCompoundTag("output"));
		toSend.setBoolean("DamageSensitive", Sensitive);
		return toSend;

	}

	public static void DeleteCraftingRecipe(ItemStack par1ItemStack) {
		List recipes = CraftingManager.getInstance().getRecipeList();

		for (Iterator i = recipes.listIterator(); i.hasNext();) {
			IRecipe recipe = (IRecipe) i.next();
			ItemStack is = recipe.getRecipeOutput();

			if (is != null)
				if (is.itemID == par1ItemStack.itemID)
					i.remove();
		}
	}

	public static void DeleteSmeltingRecipe(ItemStack Par1ItemStack) {
		Map<List<Integer>, ItemStack> recipesMeta = FurnaceRecipes.smelting()
				.getMetaSmeltingList();
		Map<Integer, ItemStack> recipes = FurnaceRecipes.smelting()
				.getSmeltingList();
		if (Par1ItemStack.isItemDamaged()
				&& recipesMeta.containsKey(Arrays.asList(Par1ItemStack.itemID,
						Par1ItemStack.getItemDamage())))
			recipesMeta.remove(Arrays.asList(Par1ItemStack.itemID,
					Par1ItemStack.getItemDamage()));

		if (!Par1ItemStack.isItemDamaged()
				&& recipes.containsKey(Par1ItemStack.itemID))
			recipes.remove(Par1ItemStack.itemID);
	}

	public static void delitem(Item par1Item) {
		DeleteCraftingRecipe(new ItemStack(par1Item));
	}

	public static File getconfigfile(File configdir, String filename) {
		File configfile = new File(configdir, filename + ".cfg");
		return configfile;
	}

	public static File getconfigfile(File configdir, String dir, String filename) {
		File configfile = new File(new File(configdir, dir), filename + ".cfg");
		return configfile;
	}

	public static File getconfigfile(String filename) {
		File configfile = new File(configdir, filename + ".cfg");
		return configfile;
	}

	public static File getconfigfile(String dir, String filename) {
		File configfile = new File(new File(configdir, dir), filename + ".cfg");
		return configfile;
	}

	public static ItemStack getOreDicItem(String OreDicName, String MODName) {
		ItemStack ItemS = null;
		ArrayList<ItemStack> OreDicL = OreDictionary.getOres(OreDicName);
		if (OreDicL.size() > 0) {
			boolean f = false;
			for (int i = 0; i < OreDicL.size();) {
				ItemStack ItemStemp = new ItemStack(OreDicL.get(i).getItem(),
						1, OreDicL.get(i).getItemDamage());
				String temp = ItemStemp.getUnlocalizedName();
				String[] temp1 = temp.split(":");
				if (temp1[0].indexOf(MODName) != -1) {
					ItemS = ItemStemp;
					f = true;
				}
				if (f)
					i = OreDicL.size();
				else
					i++;
			}
			if (ItemS == null)
				ItemS = new ItemStack(OreDicL.get(0).getItem(), 1, OreDicL.get(
						0).getItemDamage());
		}
		return ItemS;
	}
	
	@EventHandler
	private void preload(FMLPreInitializationEvent Event) {
		configdir = Event.getModConfigurationDirectory();
	}
}
